# 3bld

Simple frontend app in pure HTML/JS/CSS to help with learning 3x3x3 rubik's blindfolded

## Setup

```
python3 -m venv venv
.\venv\Scripts\activate # windows
venv/bin/activate # linux / macOS

pip3 install -r requirements.txt
```

## Run
python server