import sys
import logging
from flask import Flask, url_for, redirect, jsonify
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route("/")
def redirect_to_index_html():
    return redirect("/static/index.html")
    

if __name__ == "__main__":
    app.run(debug=True, port=1234)
    url_for("static", filename="index.html")
