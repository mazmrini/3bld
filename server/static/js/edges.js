import { Color, Edge } from './colors.js';

const lettersMap = {
    [Color.WHITE.value]: {
        [Color.BLUE.value]: 'A',
        [Color.RED.value]: 'B',
        [Color.GREEN.value]: 'C',
        [Color.ORANGE.value]: 'D',
    },
    [Color.ORANGE.value]: {
        [Color.WHITE.value]: 'E',
        [Color.GREEN.value]: 'F',
        [Color.YELLOW.value]: 'G',
        [Color.BLUE.value]: 'H',
    },
    [Color.GREEN.value]: {
        [Color.WHITE.value]: 'I',
        [Color.RED.value]: 'J',
        [Color.YELLOW.value]: 'K',
        [Color.ORANGE.value]: 'L',
    },
    [Color.RED.value]: {
        [Color.WHITE.value]: 'M',
        [Color.BLUE.value]: 'N',
        [Color.YELLOW.value]: 'O',
        [Color.GREEN.value]: 'P',
    },
    [Color.BLUE.value]: {
        [Color.WHITE.value]: 'Q',
        [Color.ORANGE.value]: 'R',
        [Color.YELLOW.value]: 'S',
        [Color.RED.value]: 'T',
    },
    [Color.YELLOW.value]: {
        [Color.GREEN.value]: 'U',
        [Color.RED.value]: 'V',
        [Color.BLUE.value]: 'W',
        [Color.ORANGE.value]: 'X',
    },
};

const allEdges = [
    new Edge(Color.WHITE, Color.BLUE),
    new Edge(Color.WHITE, Color.RED),
    new Edge(Color.WHITE, Color.GREEN),
    new Edge(Color.WHITE, Color.ORANGE),
    new Edge(Color.YELLOW, Color.GREEN),
    new Edge(Color.YELLOW, Color.RED),
    new Edge(Color.YELLOW, Color.BLUE),
    new Edge(Color.YELLOW, Color.ORANGE),
    new Edge(Color.GREEN, Color.RED),
    new Edge(Color.GREEN, Color.ORANGE),
    new Edge(Color.BLUE, Color.ORANGE),
    new Edge(Color.BLUE, Color.RED),
];


const edgesRow = document.querySelector("#edges-choice-row");
const edgeStep = document.querySelector("#memo-edge-step");
const memoEdge = document.querySelector("#memo-edges");
const missingEdges = document.querySelector("#missing-edges");
const backBtn = document.querySelector("#back-edges");
const validateBtn = document.querySelector("#edges-validate");

export const onEdgesChange = (state) => {
    var renderEdges = renderFirstEdges;
    var backOnClick = () => state.backFirstEdgeMemo();
    var tooltip = ''
    if (!state.isEdgeFirstStep) {
        tooltip = `(${state.selectedEdgeColor.displayName})`;
        renderEdges = renderSelectedEdges;
        backOnClick = () => state.backSecondEdgeMemo();
    }

    edgeStep.textContent  = tooltip;
    backBtn.onclick = backOnClick;
    validateBtn.onclick = () => {
        state.validateEdges(findMissingEdges(state));
    };
    renderEdges(state);
    renderMemo(state);
    renderMissingEdges(state);
};

function renderMemo(state) {
    const letters = state.edgesMemo.map((edge) => lettersMap[edge.color1.value][edge.color2.value]).join('');
    const memos = letters.match(/.{1,2}/g) || [];

    memoEdge.textContent = memos.join(' ');
}

function renderFirstEdges(state) {
    edgesRow.innerHTML = null;
    Color.all().forEach((color) => {
        const div = createEdgeDiv(color, () => {
            state.selectFirstEdgeColor(color);
        });

        edgesRow.appendChild(div);
    });
}

function renderSelectedEdges(state) {
    edgesRow.innerHTML = null;
    const availableColors = lettersMap[state.selectedEdgeColor.value];
    [...Object.keys(availableColors)].sort().forEach((colorValue) => {
        const color = Color.from(colorValue);
        const div = createEdgeDiv(color, () => {
            state.onEdgesMemo(new Edge(state.selectedEdgeColor, color));
        });

        edgesRow.appendChild(div);
    });
}

function createEdgeDiv(color, onClick) {
    const div = document.createElement("div");
    div.className = `edge ${color.css}`;
    div.onclick = onClick;

    return div;
}

function renderMissingEdges(state) {
    const errorText = state.missingEdges.map((edge) => edge.displayName()).join(", ");

    missingEdges.textContent = errorText;
}

function findMissingEdges(state) {
    return allEdges.filter((edge) => {
        return !state.edgesMemo.some((memoEdge) => edge.isSameAs(memoEdge));
    });
}
