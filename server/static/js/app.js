import { onEdgesChange } from './edges.js';
import { State } from './state.js';

const clearBtn = document.querySelector("#clear-btn");

const onMemoSetupChange = (state) => {
    onEdgesChange(state);
};

const onChange = (state) => {
    onMemoSetupChange(state);

    clearBtn.onclick = () => {
        state.clear();
    };

    console.log(state);
}

const state = new State(onChange);
