export class Color {
    constructor(value, css, displayName) {
        this.value = value;
        this.css = css;
        this.displayName = displayName;
    }

    static WHITE = new Color('w', 'white', 'White');
    static YELLOW = new Color('y', 'yellow', 'Yellow');
    static BLUE = new Color('b', 'blue', 'Blue');
    static GREEN = new Color('g', 'green', 'Green');
    static ORANGE = new Color('o', 'orange', 'Orange');
    static RED = new Color('r', 'red', 'Red');

    static all() {
        return [Color.WHITE, Color.YELLOW, Color.BLUE, Color.GREEN, Color.ORANGE, Color.RED]
    }

    static from(value) {
        return {
            w: Color.WHITE,
            y: Color.YELLOW,
            b: Color.BLUE,
            g: Color.GREEN,
            o: Color.ORANGE,
            r: Color.RED
        }[value];
    }
}

export class Edge {
    constructor(color1, color2) {
        this.color1 = color1;
        this.color2 = color2;
    }
    
    isSameAs(other) {
        const sameColors = this.color1 === other.color1 && this.color2 === other.color2;
        const invertedColors = this.color1 === other.color2 && this.color2 === other.color1;

        return sameColors || invertedColors;
    }

    displayName() {
        return `${this.color1.displayName}/${this.color2.displayName}`;
    }
}
