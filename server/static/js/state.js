const edges = "Edges";
const corners = "Corners";

export class State {
    constructor(onChange) {
        this.onChange = onChange;

        this.clear();
    }

    selectFirstEdgeColor(color) {
        this.selectedEdgeColor = color;
        this.isEdgeFirstStep = false;

        this.onChange(this);
    }

    onEdgesMemo(edge) {
        this.edgesMemo.push(edge);
        this.isEdgeFirstStep = true;

        this.onChange(this);
    }

    backFirstEdgeMemo() {
        if (this.edgesMemo.length === 0) {
            return;
        }

        this.edgesMemo = this.edgesMemo.slice(0, -1);
        this.isEdgeFirstStep = !this.isEdgeFirstStep;

        this.onChange(this);
    }

    backSecondEdgeMemo() {
        this.isEdgeFirstStep = !this.isEdgeFirstStep;

        this.onChange(this);
    }

    doneEdgesMemo() {
        this.isEdgesMemoDone = true;
        this.isCornerFirstStep = true;

        this.onChange(this);
    }

    validateEdges(missingEdges) {
        this.missingEdges = missingEdges;

        this.onChange(this);
    }

    onCornersMemo(memo) {
        this.cornersMemo += memo;
        this.isCornerFirstStep = !this.isCornerFirstStep;

        this.onChange(this);
    }

    backCornersMemo() {
        if (this.cornersMemo.length > 0) {
            this.cornersMemo = this.cornersMemo.slice(0, -1);
            this.isCornerFirstStep = !this.isCornerFirstStep;
        }
        else {
            this.isEdgesMemoDone = false;
            this.isEdgeFirstStep = true;
        }

        this.onChange(this);
    }

    doneCornersMemo() {
        this.isCornersMemoDone = true;
        this.isCornerFirstStep = false;

        this.onChange(this);
    }

    validateCornersMemo(missingCorners) {
        this.missingCorners = missingCorners;

        this.onChange(this);
    }

    clear() {
        this.isEdgeFirstStep = true;
        this.selectedEdgeColor = null;
        this.edgesMemo = [];
        this.isEdgesMemoDone = false;
        this.missingEdges = [];

        this.isCornerFirstStep = false;
        this.selectedCorner = null;
        this.cornersMemo = [];
        this.isCornersMemoDone = false;
        this.missingCorners = [];

        this.onChange(this);
    }
}
